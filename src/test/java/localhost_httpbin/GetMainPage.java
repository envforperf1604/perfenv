package localhost_httpbin;

import io.gatling.javaapi.core.*;
import io.gatling.javaapi.http.*;

import static io.gatling.javaapi.http.HttpDsl.*;
import static io.gatling.javaapi.core.CoreDsl.*;

public class GetMainPage extends Simulation {
    private HttpProtocolBuilder HTTP = http
            .baseUrl("http://localhost")
            .acceptHeader("application/json");

    private static ChainBuilder getMainPage = exec(
            http("main page")
                    .get("/get")
    );

    private static ScenarioBuilder scn = scenario("Debug scn").exec(
            getMainPage
    );

    {
        setUp(scn
                .injectOpen(atOnceUsers(1))
                .protocols(HTTP));
    }

}
