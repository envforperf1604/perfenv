package gatlinapi;

import java.time.Duration;
import java.util.*;

import io.gatling.javaapi.core.*;
import io.gatling.javaapi.http.*;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.*;

public class GatlingApiSimulation extends Simulation {

  private HttpProtocolBuilder httpProtocol = http
    .baseUrl("https://demostore.gatling.io")
    .header("Cache-Control","no-cache")
    .contentTypeHeader("application/json")
    .acceptHeader("application/json");
  
  private static Map<CharSequence, String> authHeaders = Map.ofEntries(
    Map.entry("authorization", "Bearer #{jwt_token}")
  );

  private static ChainBuilder initSession = exec(
          session -> session.set("authenticated",false)
  );

  private static class Auth {
    private static ChainBuilder auth =
            doIf(session -> !session.getBoolean("authenticated")).then(
                    exec(
                            http("Authorization")
                                    .post("/api/authenticate")
                                    .body(StringBody("{\"username\": \"admin\",\"password\": \"admin\"}"))
                                    .check(status().is(200))
                                    .check(jmesPath("token").saveAs("jwt_token")))
                            .exec(session -> session.set("authenticated",true))
            );
  }

  private static class Categories {
    private static final FeederBuilder.Batchable <String> feederForCategories = csv("data/categoriesApi.csv").random();
    private static final ChainBuilder list = exec(
            http("List of categories")
                    .get("/api/category")
                    .check(jmesPath("[? id == `6`].name").ofList().is(List.of("For Her")))
    );
    private static final ChainBuilder update = exec(
            feed(feederForCategories),
            Auth.auth,
            http("Update category name")
                    .put("/api/category/#{categoryId}")
                    .headers(authHeaders)
                    .body(StringBody("{\"name\": \"#{categoryName}\"}"))
                    .check(jmesPath("name").isEL("#{categoryName}"))
    );
  }

  private static class Product {
    private static FeederBuilder.Batchable<String> productsFeeder = csv("data/productsApi.csv").circular();
    private static ChainBuilder list = exec(
            http("List of product in category_X")
                    .get("/api/product?category=7")
                    .check(jmesPath("[? categoryId != '7']").ofList().is(Collections.emptyList()))
                    .check(jmesPath("[*].id").ofList().saveAs("allProductIds")));
    private static ChainBuilder listAll = exec(
            http("List all product")
                    .get("/api/product")
                    .check(jmesPath("[*]").ofList().saveAs("allProducts")));
    private static ChainBuilder get =exec(
            exec(session -> {
              List<Integer> allProductIds = session.getList("allProductIds");
              return session.set("productId", allProductIds.get(new Random().nextInt(allProductIds.size())));
            }),
            http("Detailed info about product_#{productId}")
                    .get("/api/product/#{productId}")
                    .check(jmesPath("id").ofInt().isEL("#{productId}"))
                    .check(jmesPath("@").ofMap().saveAs("product"))
    );
    private static ChainBuilder update = exec(
            Auth.auth,
            exec(session -> {
              Map<String, Object> product = session.getMap("product");
              return session
                      .set("productCategoryId",product.get("categoryId"))
                      .set("productName", product.get("name"))
                      .set("productDescription", product.get("description"))
                      .set("productImage", product.get("image"))
                      .set("productPrice", product.get("price"))
                      .set("productId", product.get("id"));
            }),
            http("update product #{productName}")
                    .put("/api/product/#{productId}")
                    .headers(authHeaders)
                    .body(ElFileBody("gatlinapi/gatlingapisimulation/create-product.json"))
                    .check(jmesPath("price").isEL("#{productPrice}"))
    );
    private static ChainBuilder create = exec(
            Auth.auth,
            feed(productsFeeder),
            http("create product #{productName}")
              .post("/api/product")
              .headers(authHeaders)
              .body(ElFileBody("gatlinapi/gatlingapisimulation/create-product.json"))
    );
  }

  private static class UserJourneys {
    private static Duration minPause = Duration.ofMillis(200);
    private static Duration maxPause = Duration.ofMillis(200);
    public static ChainBuilder admin = exec(
            initSession,
            Categories.list,
            Product.list,
            Product.get,
            Product.update,
            repeat(3).on(Product.create),
            Categories.update);

    public static ChainBuilder priceScrapper = exec(
            Product.list,
            Product.listAll
    );

    public static ChainBuilder priceUpdater = exec(
            initSession,
            Product.listAll,
            repeat("#{allProducts.size()}","productIndex").on(
                    exec(session -> {
                      int index = session.getInt("productIndex");
                      List<Object> allProducts = session.getList("allProducts");
                      return session.set("product",allProducts.get(index));
                    })
            ),
            Product.update
    );
  }

  private static class Scenarios {
      public static ScenarioBuilder defaultScn = scenario("Default load test")
              .during(Duration.ofSeconds(60))
              .on(
                      randomSwitch().on(
                             new Choice.WithWeight(20,UserJourneys.admin),
                              new Choice.WithWeight(40,UserJourneys.priceScrapper),
                              new Choice.WithWeight(40,UserJourneys.priceUpdater)
                      )
              );

      public static ScenarioBuilder noAdminsScn = scenario("Load test without Admin users")
              .during(Duration.ofSeconds(60))
              .on(
                      randomSwitch().on(
                              new Choice.WithWeight(60,UserJourneys.priceScrapper),
                              new Choice.WithWeight(40,UserJourneys.priceUpdater)
                      )
              );
  }

  private ScenarioBuilder scn = scenario("GatlingApiSimulation").exec(
          initSession,
      Categories.list,
      Product.list,
      Product.get,
      Product.update,
      repeat(3).on(Product.create),
      Categories.update
    );

  {
	  setUp(Scenarios.defaultScn.injectOpen(atOnceUsers(20))
              .protocols(httpProtocol));
  }
}
