package myexp;

import io.gatling.javaapi.core.ScenarioBuilder;
import io.gatling.javaapi.core.Simulation;
import io.gatling.javaapi.http.HttpProtocolBuilder;

import java.time.Duration;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.http.HttpDsl.http;

public class Forever extends Simulation {
    private static final HttpProtocolBuilder httpProtocol = http
            .baseUrl("https://ya.ru")
            .header("Cache-Control","no-cache")
            .contentTypeHeader("application/json")
            .acceptHeader("application/json");

    private ScenarioBuilder scn = scenario("Forever send request for 1 minute").forever().on(
            http("ya.ru")
                    .get("/")
    );

    {
        setUp(scn.injectOpen(constantUsersPerSec(0.1).during(Duration.ofMinutes(1))))
                .protocols(httpProtocol)
                .throttle(
                        reachRps(5).in(Duration.ofMinutes(1)),
                        holdFor(Duration.ofMinutes(1)),
                        reachRps(10).in(Duration.ofMinutes(1)),
                        holdFor(Duration.ofMinutes(1))
                )
                .maxDuration(Duration.ofMinutes(5));
    }
}
