package gatlingdemostore.pageobjects;

import io.gatling.javaapi.core.ChainBuilder;

import static io.gatling.javaapi.core.CoreDsl.*;
import static io.gatling.javaapi.core.CoreDsl.exec;
import static io.gatling.javaapi.http.HttpDsl.http;

public class CmsPages {
    public static final ChainBuilder homepage = exec(
            http("HomePage")
                    .get("/")
                    .check(regex("<title>Gatling Demo-Store</title>").exists())
                    .check(css("#_csrf","content").saveAs("csrfValue")));

    public static final ChainBuilder aboutUs = exec(
            http("AboutStore")
                    .get("/about-us")
                    .check(substring("About Us"))
    );
    public static final ChainBuilder selectProduct = exec(
            http("SelectProduct")
                    .get("/product/black-and-red-glasses")
    );

    public static final ChainBuilder addToCart = exec(
            http("SelectProduct")
                    .get("/product/black-and-red-glasses")
    );
}
